# frozen_string_literal: true

# See https://docs.gitlab.com/ee/development/migration_style_guide.html
# for more information on how to write migrations for GitLab.

class AddGithubForksCountToProject < Gitlab::Database::Migration[1.0]
  include Gitlab::Database::MigrationHelpers

  disable_ddl_transaction!

  def up
    with_lock_retries do
      add_column :projects, :github_forks_count, :integer, default: 0, null: false
    end
  end

  def down
    with_lock_retries do
      remove_column :projects, :github_forks_count
    end
  end
end

